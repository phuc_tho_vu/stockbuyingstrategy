import java.util.Arrays;
import java.util.Random;

import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.functions.LibSVM;
import weka.core.Instances;
import weka.core.Utils;
import weka.core.converters.ConverterUtils.DataSource;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;

/*
 * This class is responsible for building and evaluating classification model
 * 
 * It performs a single run of cross-validation (1 seed, 3 folds)
 * 
 * Command-line parameters:
 * <ul>
 * 		<li> -t filename - the dataset to use;  
 * 		file must be put in folder data\Weka_input</li>
 * 		<li> -c int - class index, "first" and "last" are accepted as well;
 * 		"last" is used as default </li>
 * 		<li> -R int[] - attributes' indexes to ignore during classification, 
 * 		enclosed by double quotes;
 * 		By default, attributes at indexes 1, 2, 3, 12 are removed already
 * 		<li> -W classifier - classname and options, enclosed by double quotes;
 * 		the classifier to cross-validate</li>
 *  </ul>
 *  
 *  Example command-line:
 *  <pre>
 *  	java WekaClassifier -t data.csv -c last -R "5, 7" -W "weka.classifiers.trees.J48 -C 0.25"
 *  </pre>
 */

public class WekaClassifier {
	private static final String DEFAULT_INPUT = "data\\Weka_input\\";
	private static final String DEFAULT_REMOVE_ATTRIBUTES = "1, 2, 3, 12";
	private Instances data;
	private Instances filted_data;
	private Instances rand_data;
	private Filter filter;
	private Classifier model;
	private int seed = 1;
	private int fold = 3;
	private Evaluation eval;
	private String classfier_name;
	
	public WekaClassifier() {
	}
	
	public static void main(String[] args) throws Exception {
		WekaClassifier classifier = new WekaClassifier();
		// set data used for classification
		classifier.setInstance(Utils.getOption("t", args));
		// set which attribute is used as class
		classifier.setClassIndex(Utils.getOption("c", args));
		// set filter to remove unwanted attributes
		// by default, the attributes below won't be considered:
		// Id, Firm, Date
		classifier.setFilter(Utils.getOption("R", args));
		// set classifier model
		classifier.setClassifierModel(Utils.getOption("W", args));
		// filter data, randomize data and evaluate 
		classifier.filterData();
		classifier.randomize();
		classifier.evaluate();
		// get report
		System.out.println(classifier.getDetailResult());
	}
	
	public void setInstance(String _input) throws Exception {
		_input = WekaClassifier.DEFAULT_INPUT + _input;
		data = DataSource.read(_input);
	} 
	
	public void setClassIndex(String _arg) throws Exception {
		if (_arg.length()==0) _arg="last";
		if (_arg=="last") {
			data.setClassIndex(data.numAttributes()-1);
		} else if (_arg=="first") {
			data.setClassIndex(0);
		} else {
			data.setClassIndex(Integer.parseInt(_arg)-1);
		}
	}
	
	public void setFilter(String _arg) throws Exception {
		String remove_attr = WekaClassifier.DEFAULT_REMOVE_ATTRIBUTES;;
		if (_arg.length()>0) {
			remove_attr += ", " + _arg;
		} 
		//System.out.println(remove_attr);
		String[] options = {"-R", remove_attr};
		Remove remove = new Remove();
		remove.setOptions(options);
		filter = remove;
	}
	
	public void setClassifierModel(String _arg) throws Exception {
		classfier_name = _arg;
		String[] options = Utils.splitOptions(_arg);
		String classname = options[0];
		options[0]="";
		//System.out.println(Arrays.toString(options));
		model = (Classifier) Utils.forName(Classifier.class, classname, options);
	}
	
	public void filterData() throws Exception {
		filter.setInputFormat(data);
		filted_data = Filter.useFilter(data, filter);
		filted_data.setClassIndex(filted_data.numAttributes()-1);
		//System.out.println(filted_data.classAttribute());
	}
	
	public void randomize() throws Exception{
		Random rand = new Random(seed);
		rand_data = new Instances(filted_data);
		rand_data.randomize(rand);
		if (rand_data.classAttribute().isNominal()) 
			rand_data.stratify(fold);
	}
	
	public void evaluate() throws Exception {
		eval = new Evaluation(rand_data);
		for (int i=0; i<fold; i++) {
			Instances train = rand_data.trainCV(fold, i);
			Instances test = rand_data.testCV(fold, i);
			
			Classifier copy = Classifier.makeCopy(model);
			copy.buildClassifier(train);
			eval.evaluateModel(copy, test);
		}
	}
	
	// get name of classifier e.g. SMO 
	public String getClassifierName() throws Exception {
		return this.classfier_name;
	}
	
	// get name of class e.g "label_c=0.001"
	public String getClassName() throws Exception {
		return data.classAttribute().toString();
	}
	
	public String getSummaryResult() throws Exception {
		return eval.toSummaryString();
	}
	
	public String getDetailResult() throws Exception {
		return eval.toClassDetailsString();
	}
	
	public String getAUC() throws Exception  {
		return String.format("%.4f", eval.weightedAreaUnderROC());
	}
	
	public String getTruePositiveRate() throws Exception {
		return String.format("%.4f", eval.weightedTruePositiveRate());
	}
	
	public String getTrueNegativeRate() throws Exception {
		return String.format("%.4f", eval.weightedTrueNegativeRate());
	}
	
	public String getFalsePositiveRate() throws Exception {
		return String.format("%.4f", eval.weightedFalsePositiveRate());
	}
	
	public String getFalseNegativeRate() throws Exception {
		return String.format("%.4f", eval.weightedFalseNegativeRate());
	}
	
	public String getFMeasure() throws Exception {
		return String.format("%.4f", eval.weightedFMeasure());
	}
	
	public String getPrecision() throws Exception {
		return String.format("%.4f", eval.weightedPrecision());
	}
	
	public String getRecall() throws Exception {
		return String.format("%.4f", eval.weightedRecall());
	}
}