import java.util.List;

/*
 * This class is responsible for providing statistics formula
 */
public class Statistics {
	public static double stdev(List<Double> values) throws Exception {
		double result = 0;
		double mean = mean(values);
		int size = values.size();
		for (int i=0; i<size; i++) {
			result += Math.pow(values.get(i)-mean, 2)/ size;
		}
		result = Math.sqrt(result);
		return result;
	}
	
	public static double mean(List<Double> values) throws Exception {
		double result = 0;
		int size = values.size();
		for (int i=0; i<size; i++) {
			result += values.get(i)/size;
		}
		return result;
	}
}
