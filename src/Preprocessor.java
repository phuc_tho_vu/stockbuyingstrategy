import java.io.*;
import java.time.LocalDate;
import java.util.*;
import java.util.Map.Entry;

import weka.core.Utils;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

/*
 * This class is responsible for preprocessing input data for classification
 * 
 * It reads input CSV file, extract data (RET, label) and save preprocessed data
 * into ARFF format (for running Weka) and CSV format (to record)
 * 
 * Command-line parameters:
 * <ul>
 * 		<li> -i filename - input CSV file;
 * 		the file must be put in folder "data\\raw_data\\" </li>
 * 		<li> -o filename - output CSV file;
 * 		the file will be put in folder "data\\Weka_input\\" </li>
 * 		<li> -d int - number of days for return value (RET). 
 * 		By default, RET with d=1 is always calculated </li>
 * 		<li> -c double - value to generate cutoff increment, "stdev" is also accepted;
 * 		"stdev" (standard deviation) is used as default </li> 
 * </ul>
 * 
 * Example command-line
 * 	java Preprocessor -i stockStrategy.csv -o data.csv -d 5 -c 0.4
 */
public class Preprocessor {
	/*
	 * @param args
	 */
	private String input;
	private String cutoff_increment;
	private int day;
	private String CSV_output;
	private String ARFF_output;
	private List<String> attributes;
	private Map<Integer, Firm> firm_records;
	private static final String DEFAULT_INPUT = "data\\raw_data\\";
	private static final String DEFAULT_OUTPUT = "data\\Weka_input\\";
	private static final int[] DEFAULT_ATTRIBUTE_INDEXES = {0, 1, 2, 8,	 12, 13, 14, 15, 16, 17, 18, 19, 9};
	
	/*
	 * @constructors
	 */
	
	public Preprocessor(String _input) {
		this.input = Preprocessor.DEFAULT_INPUT + _input;
		this.attributes = new ArrayList<String>();
		this.firm_records = new TreeMap<Integer, Firm>();
		this.day = 1;
	}
	
	/*
	 *	Main method 
	 */
	
	public static void main(String[] args) throws Exception {
		String input = Utils.getOption("i", args);
		String output = Utils.getOption("o", args);
		String cutoff = Utils.getOption("c", args);
		String day = Utils.getOption("d", args);
		Preprocessor p = new Preprocessor(input);
		p.setOutput(output);
		p.setDay(day);
		p.setCutoff(cutoff);
		
		p.preprocess();
	}

	public void setCutoff(String _increment) {
		if (_increment.length()==0) {
			cutoff_increment = "stdev";
		} else { 
			cutoff_increment = _increment;
		}
	}
	
	public void setOutput(String _output) throws Exception {
		CSV_output = Preprocessor.DEFAULT_OUTPUT + _output;
		ARFF_output = Preprocessor.DEFAULT_OUTPUT + _output.substring(0, _output.length()-3) + "arff";
	}
	
	public void setDay(String _day) throws Exception {
		if (_day.length()>0)
			this.day = Integer.parseInt(_day);
		else 
			this.day = 1;
	}
	
	public void preprocess() throws Exception{
		readCSVInput();
		extractData();
		writeToCSV();
		writeToARFF();
	}
	
	public void readCSVInput() {
		try {
			CSVReader reader = new CSVReader(new FileReader(input));
			
			String[] next_line; int count = 0;
			while ((next_line = reader.readNext()) != null) {
				count++;
				if (count==1) {
					// save attributes 
					setAttributes(next_line);
				} else {
					// save data
					setData(next_line);
				}
			}
			
			reader.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void extractData() throws Exception {
		// Extract RET of n-days
		if (day>1) { 	
			attributes.remove(attributes.size()-1);
			attributes.add("RET_"+Integer.toString(day));
			for (Entry<Integer, Firm> entry : firm_records.entrySet()) {
				Firm firm = entry.getValue();
				firm.calculateRET(day);
			}
		}
		
		// Extract label(s)
		double increment = calculateIncr();
		double cutoff = 0 - 3 * increment;
		for (int i=0; i<7; i++) {
			attributes.add(String.format("Label_c=%.4f", cutoff));
			
			for (Entry<Integer, Firm> entry : firm_records.entrySet()) {
				Firm firm = entry.getValue();
				firm.calculateLabel(cutoff);
				firm_records.put(entry.getKey(), firm);
			}
			
			cutoff += increment;
		}
	}
	
	public void writeToCSV() {
		try {
			CSVWriter writer = new CSVWriter(new FileWriter(CSV_output));
			
			// write attributes
			// ignore ADJ_Close
			List<String> attr = attributes;
			attr.remove(3);
			writer.writeNext(attr.toArray(new String[attributes.size()]));
	
			//write pre-processed data
			for (Entry<Integer, Firm> firm_entry : firm_records.entrySet()) {
				Firm firm = firm_entry.getValue();
				for (Entry<Integer, DailyRecord> record_entry : firm.getRecords().entrySet()) {
					DailyRecord record = record_entry.getValue();
					writer.writeNext(record.toArray(day));
				}
			}
			
			writer.close();	
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void writeToARFF() {
		try {
			File file = new File(ARFF_output);
			if (!file.exists()) file.createNewFile();
			FileWriter fw = new FileWriter(file.getAbsolutePath());
			BufferedWriter bw = new BufferedWriter(fw);
			
			bw.write(ArffFormatter.relation("Financial daily records"));
			bw.newLine();
			for (String attribute :attributes) {
				if (attribute.compareTo("Date")==0) {
					bw.write(ArffFormatter.attribute(attribute, ArffFormatter.AttrType.DATE));
				} else if (attribute.contains("Label")) {
					String[] choices = {"0", "1"};
					bw.write(ArffFormatter.attribute(attribute, choices, ArffFormatter.AttrType.NOMINAL));
				} else {
					bw.write(ArffFormatter.attribute(attribute, ArffFormatter.AttrType.NUMERIC));
				}
			}
			bw.newLine();
			bw.write(ArffFormatter.data());
			for (Entry<Integer, Firm> firm_entry : firm_records.entrySet()) {
				Firm firm = firm_entry.getValue();
				for (Entry<Integer, DailyRecord> record_entry : firm.getRecords().entrySet()) {
					// ignore record with no value of RET (i.e. -1)
					DailyRecord record = record_entry.getValue();
					if (record.getRET(day)!=-1) {
						String[] data_row = record.toWekaArray(day);
						// change date to required format in Weka arff file 
						data_row[2] = "\"" + data_row[2] + "\"";
						bw.write((ArffFormatter.data(data_row)));
					}
				}
			}
			
			bw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void setAttributes(String[] _raw_attr) {
		for (int index : Preprocessor.DEFAULT_ATTRIBUTE_INDEXES) {
			attributes.add(_raw_attr[index]);	
		}
	}
	
	private void setData(String[] _raw_data) {
		String[] data = new String[Preprocessor.DEFAULT_ATTRIBUTE_INDEXES.length];
		for (int i=0; i<Preprocessor.DEFAULT_ATTRIBUTE_INDEXES.length; i++) {
			int index = Preprocessor.DEFAULT_ATTRIBUTE_INDEXES[i];
			data[i] = _raw_data[index];
		}
		String firm_id = data[1];
		if (firm_id.length()>0) {
			Firm firm = firm_records.get(Integer.parseInt(firm_id));
			if (firm==null) {
				firm = new Firm(firm_id);
			}
			firm.addRecord(data);
			firm_records.put(Integer.parseInt(firm_id), firm);
		}
	}
	
	private double calculateIncr() throws Exception{
		if (cutoff_increment.compareTo("stdev")==0) {
			System.out.println(day);
			return calculateStdev(day);
		} else {
			return Double.parseDouble(cutoff_increment);
		}
	}
	
	private double calculateStdev(int day) throws Exception {
		List<Double> RET = new ArrayList<Double>();
		
		for (Entry<Integer, Firm> firm_entry : firm_records.entrySet()) {
			Firm firm = firm_entry.getValue();
			for (Entry<Integer, DailyRecord> record_entry : firm.getRecords().entrySet()) {
				if (record_entry.getValue().getRET(day)!=-1)
					RET.add(record_entry.getValue().getRET(day));
			}
		}
		
		double result = Statistics.stdev(RET);
		return result;
	}
}