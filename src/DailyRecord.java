import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

public class DailyRecord {
	private int id;
	private int firm;
	private LocalDate date;
	private double ADJ_Close;
	private double KD9K;
	private double KD9D;
	private double RSI10;
	private double top10;
	private double top20;
	private double gini;
	private double ninety_pct;
	private double eighty_pct;
	private Map<Integer, Double> RETs;
	private Map<Integer, Map <Double, Integer>> labels; 
	
	public DailyRecord() {
		RETs = new TreeMap<Integer, Double>();
		labels = new TreeMap<Integer, Map <Double, Integer>>();
	}
	
	public void setData(String[] data) {
		try {
			setID(data[0]);
			setFirm(data[1]);
			setDate(data[2]);
			setADJ_Close(data[3]);
			setKD9K(data[4]);
			setKD9D(data[5]);
			setRSI10(data[6]);
			setTop10(data[7]);
			setTop20(data[8]);
			setGini(data[9]);
			setNinetyPct(data[10]);
			setEightyPct(data[11]);
			setRET(data[12], 1);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void setID(String _id) {
		this.id = Integer.parseInt(_id);
	}
	
	public void setFirm(String _firm) {
		this.firm = Integer.parseInt(_firm);
	}
	
	public void setDate(String _date) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/M/yyyy");
		this.date = LocalDate.parse(_date, formatter);
	}
	
	public void setADJ_Close(String _ADJ_Close) {
		this.ADJ_Close = parseDouble(_ADJ_Close);
	}
	
	public void setKD9K(String _KD9K) {
		this.KD9K = parseDouble(_KD9K);
	}
	
	public void setKD9D(String _KD9D) {
		this.KD9D = parseDouble(_KD9D);
	}
	
	public void setRSI10(String _RSI10) {
		this.RSI10 = parseDouble(_RSI10);
	}
	
	public void setTop10(String _top10) {
		this.top10 = parseDouble(_top10);
	}
	
	public void setTop20(String _top20) {
		this.top20 = parseDouble(_top20);
	}
	
	public void setGini(String _gini) {
		this.gini = parseDouble(_gini);
	}
	
	public void setNinetyPct(String _ninety_pct) {
		this.ninety_pct = parseDouble(_ninety_pct);
	}
	
	public void setEightyPct(String _eighty_pct) {
		this.eighty_pct = parseDouble(_eighty_pct);
	}
	
	public void setRET(String _RET, int _day) {
		if (_RET.length()>0) {
			RETs.put(_day, parseDouble(_RET.substring(0, _RET.length()-1))/100);
		} else {
			RETs.put(_day, -1.0);
		}
	}
	
	public void setRET(Double _RET, int _day) {
		RETs.put(_day, _RET);
	}
	
	public LocalDate getDate() {
		return this.date;
	}
	
	public Double getADJClose() {
		return this.ADJ_Close;
	}
	
	public Double getRET(int _day) {
		return this.RETs.get(_day);
	}
	
	public String[] toArray() {
		int size = 11 + RETs.size();
		for (Entry<Integer, Map<Double, Integer>> label_entry : labels.entrySet()) {
			size += label_entry.getValue().size();
		}
		String[] result = new String[size];
		
		result[0] = String.format("%d", id);
		result[1] = String.format("%d", firm);
		result[2] = date.toString();
		result[3] = String.format("%.3f", KD9K);
		result[4] = String.format("%.3f", KD9D);
		result[5] = String.format("%.3f", RSI10);
		result[6] = String.format("%.3f", top10);
		result[7] = String.format("%.3f", top20);
		result[8] = String.format("%.3f", gini);
		result[9] = String.format("%.3f", ninety_pct);
		result[10] = String.format("%.3f", eighty_pct);
		
		int index = 11;
		for (Entry<Integer, Double> entry : RETs.entrySet()) {
			if (entry.getValue()!=-1)
				result[index] = String.format("%.3f", entry.getValue());
			else 
				result[index] = "";
			index++;
		}
		
		for (Entry<Integer, Map<Double, Integer>> label_entry : labels.entrySet()) {
			for (Entry<Double, Integer> entry : label_entry.getValue().entrySet()) {
				result[index] = Integer.toString(entry.getValue());
				index++;
			}
		}
		
		return result;
	}
	
	public String[] toArray(int day) {
		int size = 12 + labels.get(day).size();
		String[] result = new String[size];
		
		result[0] = String.format("%d", id);
		result[1] = String.format("%d", firm);
		result[2] = date.toString();
		result[3] = String.format("%.3f", KD9K);
		result[4] = String.format("%.3f", KD9D);
		result[5] = String.format("%.3f", RSI10);
		result[6] = String.format("%.3f", top10);
		result[7] = String.format("%.3f", top20);
		result[8] = String.format("%.3f", gini);
		result[9] = String.format("%.3f", ninety_pct);
		result[10] = String.format("%.3f", eighty_pct);
		
		if (RETs.get(day)!=-1.0)
			result[11] = String.format("%.3f", RETs.get(day));
		else 
			result[11] = "";
		
		int index = 12;
		Map<Double, Integer> label = labels.get(day);
		for (Entry<Double, Integer> entry : label.entrySet()) {
			result[index] = Integer.toString(entry.getValue());
			index++;
		}
		
		return result;
	}
	
	public String[] toWekaArray(int day) {
		int size = 12 + labels.get(day).size();
		String[] result = new String[size];
		
		result[0] = String.format("%d", id);
		result[1] = String.format("%d", firm);
		result[2] = date.toString();
		result[3] = String.format("%.3f", KD9K);
		result[4] = String.format("%.3f", KD9D);
		result[5] = String.format("%.3f", RSI10);
		result[6] = String.format("%.3f", top10);
		result[7] = String.format("%.3f", top20);
		result[8] = String.format("%.3f", gini);
		result[9] = String.format("%.3f", ninety_pct);
		result[10] = String.format("%.3f", eighty_pct);
		result[11] = String.format("%.3f", RETs.get(day));
		
		int index = 12;
		Map<Double, Integer> label = labels.get(day);
		for (Entry<Double, Integer> entry : label.entrySet()) {
			result[index] = Integer.toString(entry.getValue());
			index++;
		}
		
		return result;
	}
	
	public void calculateLabel(double cutoff) {
		for (Entry<Integer, Double> RET_entry : RETs.entrySet()) {
			int day = RET_entry.getKey();
			double RET = RET_entry.getValue();
			int label = (RET-cutoff>0)? 1 : 0;
			if (!labels.containsKey(day)) {
				Map<Double, Integer> entry = new TreeMap<Double, Integer>();
				entry.put(cutoff, label);
				labels.put(day, entry);
			} else {
				Map<Double, Integer> entry = labels.get(day);
				entry.put(cutoff, label);
				labels.put(day, entry);
			}
		}
		
	}
	
	public static void main(String[] args) {
		DailyRecord dr = new DailyRecord();
		dr.setDate("3/2/2012");
		dr.setRET("2.34%", 1);
		System.out.println(dr.getRET(1)); 
		String test = "Label_c=0.0002";
		int i = test.indexOf("c=");
	}
	
	private double parseDouble(String str) {
		if (str != null && str.length() > 0) {	
			try {
				return Double.parseDouble(str);
			} catch(Exception e) {
				return 0;  
		    }
		}
		return 0;
	}
}
