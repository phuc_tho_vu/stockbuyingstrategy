import java.util.*;
import java.util.Map.Entry;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.io.*;

import com.opencsv.CSVReader;

/*
 * This class is responsible for reading files 
 * containing info about classifiers.
 */

public class ClassifierReader {
	private Map<String, String> name_map;
	private String file;
	private List<String> labels;
	private List<String> classifiers;
	private final String DEFAULT_LOADER = "lib\\weka.jar";
	private final String DEFAULT_PREFIX = "weka/classifiers";
	
	
	public ClassifierReader(String _file) {
		file = _file;
		labels = new ArrayList<String>();
		classifiers = new ArrayList<String>();
		name_map = new HashMap<String, String>();
		loadName();
	}
	
	public List<String> getClassifiers() {
		return this.classifiers;
	}
	
	// a method to get full name of class, including 
	// all parental packages from its short form
	// e.g. from "SMO" to "weka.classifiers.functions.SMO"
	public String getClassName(String _arg) {
		_arg = _arg.toLowerCase();
		String result = "";
		while (name_map.containsKey(_arg)) {
			if (result.length()!=0) {
				result = "." + result; 
			} 
			result = name_map.get(_arg) + result;
			_arg = name_map.get(_arg);
		}
		
		return result;		
	}
	
	private void loadName() {
		try {
			ZipInputStream zir = new ZipInputStream(new FileInputStream(DEFAULT_LOADER));
			for (ZipEntry entry = zir.getNextEntry(); entry!=null; entry = zir.getNextEntry()) {
				if (entry.getName().startsWith(DEFAULT_PREFIX)
						&& entry.getName().endsWith(".class") 
						&& !entry.isDirectory()) {
					StringBuilder className = new StringBuilder();
					String[] parts = entry.getName().split("/");
					for (String part : parts) {
						if (className.length()!=0)
							className.append('.');
						if (part.endsWith(".class")) {
							part = part.substring(0, part.length()-".class".length());
						}
						className.append(part);
					}
					// record only last part in lower case
					String last = parts[parts.length-1];
					last = last.substring(0, last.length()-".class".length());
					if (!name_map.containsKey(last.toLowerCase()))
						name_map.put(last.toLowerCase(), className.toString());
				}
			}
			zir.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void readClassifierFile() throws Exception {
		CSVReader cr = new CSVReader(new FileReader(file));
		String[] new_line; int count = 0;
		
		while ((new_line=cr.readNext())!=null) {
			count++;
			if (count==1) {
				// labels
				labels = Arrays.asList(new_line);
			} else {
				// data
				preprocessName(new_line);
			}
		}
		cr.close();
	}
	
	private void preprocessName(String[] array) {
		String classifier = "";
		List<String> attr = new ArrayList<String>();
		if (array.length>0) {
			// get class name
			array[0] = getClassName(array[0]);
			classifier += array[0];
			// check attributes' valid numerical form
			for (int i=1; i<array.length; i++) {
				attr.add(array[i]);
			}
			// generate attribute(s) with found cutoff and interval 
			List<List<String>> attr_list = loadAttr(attr);
			for (List<String> list : attr_list) {
				String c = classifier;
				for (int i=0; i<list.size(); i++) {
					if (labels.size()>i+1 && list.get(i).length()>0) {
						c += " -" + labels.get(i+1) +
									  " " + list.get(i).toString();
					}
				}
				classifiers.add(c);
			}
		}
	}
	
	private List<List<String>> loadAttr(List<String> attr) {
		List<List<String>> list = new ArrayList<List<String>>(); 
		list.add(attr);
		return loadAttr(list, 1);
	}
	
	private List<List<String>> loadAttr(List<List<String>> list, int index) {
		if (index==labels.size()) return list;
		String label = labels.get(index);
		if (labels.contains(label+"_cutoff") && labels.contains(label+"_interval")) {
			int cutoff_index = labels.indexOf(label+"_cutoff");
			int interval_index = labels.indexOf(label+"_interval");
			List<List<String>> new_list = new ArrayList<List<String>>();
			for (List<String> attr : list) {
				try {
					List<String> new_attr = new ArrayList<String>(attr);
					Double val = Double.parseDouble(attr.get(index-1));
					Double cutoff = Double.parseDouble(attr.get(cutoff_index-1));
					int interval = Integer.parseInt(attr.get(interval_index-1));
					new_attr.set(cutoff_index-1, "");
					new_attr.set(interval_index-1, "");
					for (int i=0; i<interval; i++) {
						new_attr.set(index-1, val.toString());
						new_list.add(new ArrayList<String>(new_attr));
						val += cutoff;
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return loadAttr(new_list, index+1);
		} 
		return loadAttr(list, index+1);
	}
	
	// only for testing
	public static void main (String[] args) throws Exception {
		String file = "data\\classifiers\\classifiers_2.csv";
		ClassifierReader cr = new ClassifierReader(file);
		// read CSV file
		cr.readClassifierFile();
 	} 
}
