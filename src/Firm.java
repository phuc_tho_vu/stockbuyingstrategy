import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

public class Firm {
	private int firm_id;
	private Map<Integer, DailyRecord> daily_records;
	private int record_size;
	
	public Firm(String _firm_id) {
		this.firm_id = Integer.parseInt(_firm_id);
		this.daily_records = new TreeMap<Integer, DailyRecord>();
		this.record_size = 0;
	}
	
	public void addRecord(String[] _record) {
		DailyRecord record = new DailyRecord();
		record.setData(_record);
		record_size++;
		daily_records.put(record_size, record);
	}
	
	public Map<Integer, DailyRecord> getRecords() {
		return this.daily_records;
	} 
	
	public void calculateLabel(double cutoff) {
		for (Entry<Integer, DailyRecord> entry : daily_records.entrySet()) {
			int key = entry.getKey();
			DailyRecord record = entry.getValue();
			record.calculateLabel(cutoff);
			daily_records.put(key, record);
		}
	}
	
	public void calculateRET(int day) {
		for (Entry<Integer, DailyRecord> entry : daily_records.entrySet()) {
			int key = entry.getKey();
			int prev = key - day;
			if (daily_records.containsKey(prev)) {
				DailyRecord dr1 = entry.getValue();
				DailyRecord dr2 = daily_records.get(prev);
				double ADJ_Close1 = dr1.getADJClose();
				double ADJ_Close2 = dr2.getADJClose();
				double RET = ADJ_Close1/ADJ_Close2 -1;
				dr1.setRET(RET, day);
				daily_records.put(key, dr1);
			} else {
				DailyRecord dr1 = entry.getValue();
				dr1.setRET(-1.0, day);
				daily_records.put(key, dr1);
			}
		}
	}
}
