public class ArffFormatter {
	public enum AttrType {
		NUMERIC, DATE, STRING, NOMINAL
	}; 
	
	public static String relation(String relation) {
		relation = verifyName(relation);
		String result = String.format("@RELATION %s\n", relation);
		return result;
	}
	
	public static String attribute(String attr, AttrType attr_type) {
		attr = verifyName(attr);
		String type = getType(attr_type);
		if (attr_type==AttrType.DATE) {
			//return String.format("@ATTRIBUTE %s STRING \n", attr);
			return String.format("@ATTRIBUTE %s %s \"yyyy-MM-dd\" \n", attr, type);
		} else {
			return String.format("@ATTRIBUTE %s %s\n", attr, type);
		}
	}
	
	public static String attribute(String attr, String[] choices, AttrType attr_type) {
		attr = verifyName(attr);
		String result = "";
		if (attr_type==AttrType.NOMINAL) {
			String list = "{";
			for (int i=0; i<choices.length; i++) {
				list+= choices[i];
				if (i+1<choices.length) {
					list+=",";
				}
			}
			list+="}";
			result = String.format("@ATTRIBUTE %s %s\n", attr, list);
		}		
		return result;
	}
	
	public static String data() {
		return "@DATA\n"; 
	}
	
	public static String data(String[] data) {
		String result = "";
		if (data.length>0) {
			for (int i=0; i<data.length; i++) {
				result+= data[i] + ",";
			}
			result = result.substring(0, result.length()-1);
		}
		
		return result + "\n";
	}
	
	private static String getType(AttrType type) {
		String result = "";
		switch (type) {
			case NUMERIC: {
				result = "NUMERIC"; break;
			} 
			case STRING: {
				result = "STRING"; break;
			}
			case DATE: {
				result = "DATE"; break;
			}
			default:
				break;
		}
		return result;
	}
	
	private static String verifyName(String str) {
		// if str starts with non-alphabetic characters
		// and end with non-alphabetic, non-digit characters
		if (str.length()>0) {
			char c = str.charAt(0);
			if (!Character.isAlphabetic(c))
				str = "'"+str+"'";
		}
		
		// if str contains space
		if (str.contains(" ")) {
			str = "'" + str + "'";
		}
		return str;
	}
}