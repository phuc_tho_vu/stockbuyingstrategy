import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

import weka.core.Utils;

/*
 * This class is responsible for evaluating Weka classifiers
 * 
 * Command-line parameters:
 * <ul>
 * 		<li> -i filename - input CSV data file;
 * 		the file must be put in folder "data\\raw_data" </li>
 * 		<li> -w filename - input CSV classifier file;
 * 		the file must be put in folder "data\\classifiers" </li>
 * 		<li> -o filename - output CSV file;
 * 		the file will be put in folder "data\\output" </li>
 * 		<li> -c int - cutoff increment value, "stdev" is accepted as well;
 * 		by default, "stdev" will be used </li>
 * 		<li> -d int - number of day for calculating RET
 * 		by default, day = 1 will be used </li>
 * </ul>
 * 
 * Example command-line:
 * <pre>
 * 		java Evaluator -i buyStrategy.csv -o report.csv -w classifiers.csv -c stdev  -d 6
 * </pre>
 */

public class Evaluator {
	private String input;
	private String output;
	private String classifier_data;
	private String cutoff;
	private String day;
	private List<String> classifiers;
	private List<String> labels;
	private List<Integer> label_indexes;
/*	private Map<String, Map<Integer, String>> AUC; // area under ROC values, mapped by label indexes and model name
	private Map<String, Map<Integer, String>> TPR; // true positive rate
	private Map<String, Map<Integer, String>> TNR;
	private Map<String, Map<Integer, String>> FPR;
	private Map<String, Map<Integer, String>> FNR;
	private Map<String, Map<Integer, String>> FMeasure;
	private Map<String, Map<Integer, String>> Precision;
	private Map<String, Map<Integer, String>> Recall;*/

	private List<WekaClassifier> c;
	
	private static final String DEFAULT_RAW_INPUT = "data\\raw_data\\";
	private static final String DEFAULT_CLASSIFIER_INPUT = "data\\classifiers\\";
	private static final String DEFAULT_PREPROCESSED_INPUT = "data\\Weka_input\\";
	private static final String DEFAULT_OUTPUT = "data\\output\\";
	private static Map<String, String> DEFAULT_CLASSIFIERS = new TreeMap<String, String>();
	
	public Evaluator() {
		input = "";
		output = "";
		cutoff = "";
		day = "";
		classifiers = new ArrayList<String>();
		labels = new ArrayList<String>();
		label_indexes = new ArrayList<Integer>();
		/*AUC = new HashMap<String, Map <Integer, String>>();
		TPR = new HashMap<String, Map <Integer, String>>();
		TNR = new HashMap<String, Map <Integer, String>>();
		FPR = new HashMap<String, Map <Integer, String>>();
		FNR = new HashMap<String, Map <Integer, String>>();
		FMeasure = new HashMap<String, Map <Integer, String>>();
		Precision = new HashMap<String, Map <Integer, String>>();
		Recall = new HashMap<String, Map <Integer, String>>();*/
		
		c = new ArrayList<WekaClassifier>();
		
		// add classifiers
		DEFAULT_CLASSIFIERS = new TreeMap<String, String>();
		DEFAULT_CLASSIFIERS.put("smo", "weka.classifiers.functions.SMO");
		DEFAULT_CLASSIFIERS.put("naivebayes", "weka.classifiers.bayes.NaiveBayes");
		DEFAULT_CLASSIFIERS.put("j48", "weka.classifiers.trees.J48");
		DEFAULT_CLASSIFIERS.put("ibk", "weka.classifiers.lazy.IBk");
		DEFAULT_CLASSIFIERS.put("randomtree", "weka.classifiers.trees.RandomTree");
	}
	
	public static void main(String[] args) throws Exception {
		Evaluator eval = new Evaluator();
		eval.setInput(Utils.getOption("i", args));
		eval.setOutput(Utils.getOption("o", args));
		eval.setClassifiers(Utils.getOption("w", args));
		eval.setCutoff(Utils.getOption("c", args));
		eval.setDay(Utils.getOption("d", args));
		eval.evaluate();
	}
	
	public void setInput(String _input) {
		input = _input;
	}
	
	public void setOutput(String _output) {
		output = _output;
	}
	
	public void setClassifiers(String _filename) {
		classifier_data = _filename;
	}
	
	public void setCutoff(String _cutoff) {
		cutoff = _cutoff;
	}
	
	public void setDay(String _day) {
		day = _day;
	}
	
	public void evaluate() throws Exception {
		// pre-process data
		Preprocessor preprocessor = new Preprocessor(input);
		preprocessor.setCutoff(cutoff);
		preprocessor.setDay(day);
		preprocessor.setOutput("data" + input);
		preprocessor.preprocess();
		
		// read file about classifiers
		ClassifierReader reader = new ClassifierReader(DEFAULT_CLASSIFIER_INPUT+classifier_data);
		reader.readClassifierFile();
		classifiers = reader.getClassifiers();
		// readClassifiers();
		
		// get all attributes (labels) used as classes
		extractClassAttributes();
		
		// run WEKA classification models and get results (weighted area under ROC)
		String Weka_input = "data" + input.substring(0, input.length()-3) + "arff";
		
		for (String model : classifiers) {
			for (int i=0; i<labels.size(); i++) {
				// set index for class
				//String class_name = labels.get(i);
				int class_index = label_indexes.get(i);
				// remove other labels during filtering
				String remove = setRemove(class_index);
				
				WekaClassifier cls = new WekaClassifier();
				cls.setClassifierModel(model);
				cls.setInstance(Weka_input);
				cls.setFilter(remove);
				cls.setClassIndex(Integer.toString(class_index));
				cls.filterData();
				cls.randomize();
				cls.evaluate();
				c.add(cls);
			}
		}
		
		/*for (String model : classifiers) {
			Map <Integer, String> aur = new HashMap<Integer, String>();
			Map <Integer, String> tpr = new HashMap<Integer, String>();
			Map <Integer, String> tnr = new HashMap<Integer, String>();
			Map <Integer, String> fpr = new HashMap<Integer, String>();
			Map <Integer, String> fnr = new HashMap<Integer, String>();
			Map <Integer, String> f_measure = new HashMap<Integer, String>();
			Map <Integer, String> precision = new HashMap<Integer, String>();
			Map <Integer, String> recall = new HashMap<Integer, String>();
			for (int i=0; i<labels.size(); i++) {
				// set index for class
				String class_name = labels.get(i);
				int class_index = label_indexes.get(i);
				// remove other labels during filtering
				String remove = setRemove(class_index);
				
				WekaClassifier classifier = new WekaClassifier();
				classifier.setClassifierModel(model);
				classifier.setInstance(Weka_input);
				classifier.setFilter(remove);
				classifier.setClassIndex(Integer.toString(class_index));
				classifier.filterData();
				classifier.randomize();
				classifier.evaluate();
				aur.put(class_index, classifier.getAUC());
				tpr.put(class_index, classifier.getTruePositiveRate());
				tnr.put(class_index, classifier.getTrueNegativeRate());
				fpr.put(class_index, classifier.getFalsePositiveRate());
				fnr.put(class_index, classifier.getFalseNegativeRate());
				f_measure.put(class_index, classifier.getFMeasure());
				precision.put(class_index, classifier.getPrecision());
				recall.put(class_index, classifier.getRecall());
				System.out.println(String.format("%s %s %s", model, 
																class_name,
																classifier.getAUC()));
			}
			AUC.put(model, aur);
			TPR.put(model, tpr);
			TNR.put(model, tnr);
			FPR.put(model, fpr);
			FNR.put(model, fnr);
			FMeasure.put(model, f_measure);
			Precision.put(model, precision);
			Recall.put(model, recall);
		}
		*/
		// create a CSV file for recording outcomes
		writeToCSV();
	}
	
	public void readClassifiers() throws Exception {
		String file = Evaluator.DEFAULT_CLASSIFIER_INPUT + classifier_data;
		
		CSVReader reader = new CSVReader(new FileReader(file));
		String[] nextLine; int count = 0;
		String[] attrName = new String[10]; 
		while ((nextLine = reader.readNext())!=null) {
			count++;
			if (count==1) {
				attrName = nextLine;
			} else {
				String classifier = getClassifierName(nextLine[0]);
				for (int i=1; i<nextLine.length; i++) {
					if (attrName.length>i && nextLine[i].length()>0) {
						classifier += " -" + attrName[i]; 
						classifier += " " + nextLine[i];
					}
				}
				classifiers.add(classifier);
			}
		} 
		reader.close();
	}
	
	private void writeToCSV() throws Exception {
		CSVWriter writer = new CSVWriter(new FileWriter(Evaluator.DEFAULT_OUTPUT + output));
		
		// write attributes
		String[] attributes = {
				"Classifier model",
				"cutoff",
				"AUC", 
				"TP",
				"TN",
				"FP",
				"FN",
				"F-Measure",
				"Precision",
				"Recall"
		};
		writer.writeNext(attributes);
		
		// write data
		String[] data_row = new String[attributes.length];
		
		for (WekaClassifier x : c) {
			data_row[0] = x.getClassifierName();
			data_row[1] = extractCutoff(x.getClassName());
			data_row[2] = x.getAUC();
			data_row[3] = x.getTruePositiveRate();
			data_row[4] = x.getTrueNegativeRate();
			data_row[5] = x.getFalsePositiveRate();
			data_row[6] = x.getFalseNegativeRate();
			data_row[7] = x.getFMeasure();
			data_row[8] = x.getPrecision();
			data_row[9] = x.getRecall();
			writer.writeNext(data_row);
		}
		
		/*for (String model: classifiers) {
			data_row[0] = model;
			for (int i=0; i<labels.size(); i++) {
				// extract cutoff values
				data_row[1] = extractCutoff(labels.get(i));
				// extract AUC value and others
				data_row[2] = AUC.get(model).get(label_indexes.get(i));
				data_row[3] = TPR.get(model).get(label_indexes.get(i));
				data_row[4] = TNR.get(model).get(label_indexes.get(i));
				data_row[5] = FPR.get(model).get(label_indexes.get(i));
				data_row[6] = FNR.get(model).get(label_indexes.get(i));
				data_row[7] = FMeasure.get(model).get(label_indexes.get(i));
				data_row[8] = Precision.get(model).get(label_indexes.get(i));
				data_row[9] = Recall.get(model).get(label_indexes.get(i));
				writer.writeNext(data_row);
			}
		}*/
		
		writer.close();
	}
	
	private void extractClassAttributes() throws Exception {
		String Weka_input = Evaluator.DEFAULT_PREPROCESSED_INPUT +
							"data" + input.substring(0, input.length()-3) + "arff";
		File file = new File(Weka_input);
		FileReader fr = new FileReader(file.getAbsolutePath());
		BufferedReader br = new BufferedReader(fr);
		String new_line = "";
		int count_attr = 0;
		while ((new_line=br.readLine())!=null) {
			if (new_line.compareTo("@DATA")==0) break;
			if (new_line.contains("@ATTRIBUTE")) {
				count_attr++;
				String[] args = new_line.split(" ");
				if (args[1].contains("Label")) {
					// update 
					labels.add(args[1]);
					label_indexes.add(count_attr);
				}
				
			}
		}
		br.close();
	}
	
	private String extractCutoff(String label) {
		String result = "";
		int index = label.indexOf("c=");
		if (index>0 && index<label.length()-7) 
			result = label.substring(index+2, index+8);
		
		return result;
	}
	
	private String setRemove(int class_index) throws Exception {
		String result = "";
		// add all labels' indexes except class_index
		for (int i=0; i<label_indexes.size(); i++) {
			if (label_indexes.get(i).compareTo(class_index)!=0)
				result += label_indexes.get(i) + ", ";
		}
		// remove last comma
		if (result.length()>0)
			result = result.substring(0, result.length()-2);
		return result;
	}
	
	private String getClassifierName(String shortname) throws Exception {
		return Evaluator.DEFAULT_CLASSIFIERS.get(shortname.toLowerCase());
	}
}