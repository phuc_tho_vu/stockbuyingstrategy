Guide to use StockPredictor application
By: Vu Phuc Tho - a0090585x

--------------------------------------------------------------
I. General Information
II. Data Folders and Files
	1. Raw_input
	2. Weka_input 
	3. Classifiers
	4. Output
III. Executable Files
 	1. Preprocessor.jar
	2. WekaClassifier.jar
	3. Evaluator.jar
IV. How to use

--------------------------------------------------------------

I. General Information

StockPredictor contains 3 main classes: Preprocessor, WekaClassifier and Evaluator to preprocess stock database, classify and compare classification's results. The program uses Weka library as core to run classification.

Source code: https://bitbucket.org/phuc_tho_vu/stockbuyingstrategy

If you have any question or problem while using program, please contact: 

Vu Phuc Tho
(P) +65 9376 9126 (E) a0090585@u.nus.edu


--------------------------------------------------------------

II. Input

1. Raw input
- This folder contains raw CSV data file for preprocessing. One sample for those file is buyStrategy.csv.

* Note: 
- If you have excel data file, always make a CSV file out of it to run the applications.
- The format of raw input CSV file must follows the sample, i.e. same attributes and data format.
	
2. Weka_input
- This folder contains preprocessed input file in CSV and ARFF format. Those files are used for classification.
	
3. Classifiers
- This folder contains CSV files of classifiers we want to use in evaluation. Two samples files are classifiers.csv and classifiers_2.csv

- File format: the CSV file must contains those following info:
	+ First row: containing labels ("Classifiers" and all 	attributes' name)
	+ Data row: containing name of classifier and its 	attributes. 
	Classifier's name is not case-sensitive.
	For attributes' cells with no value or with default 	value, we can leave them blank.   	

* Note:
- If you want to loop on one attributes (e.g. C in SMO), following the format of classifiers_2.csv: add two labels to the first row ("C_cutoff" and "C_interval"). Those two labels are case_sensitive.

4. Output
- This folder contains the CSV output file of evaluation process.

--------------------------------------------------------------
III. Executable Files
1. Preprocessor.jar
This class is responsible for preprocessing input data for classification

 It reads input CSV file, extract data (RET, label) and save preprocessed data into ARFF format (for running Weka classifier) and CSV format (to record)

Command-line parameters:
 	-i filename - input CSV file; 
	the file must be put in folder "data\\raw_data\\"
	
 	-o filename - output CSV file;
	the file will be put in folder "data\\Weka_input\\" 
	
	-d int - number of days for return value (RET). 
 	By default, RET with d=1 is always calculated

 	-c double - value to generate cutoff increment, 
	"stdev" is also accepted;
 	"stdev" (standard deviation) is used as default

 Example command-line
 java -jar Preprocessor.jar -i buyStrategy.csv -o data.csv -d 5 -c 0.4

2. WekaClassifier.jar

This class is responsible for building and evaluating classification model
 
It performs a single run of cross-validation (1 seed, 3 folds) 

Command-line parameters:
	-t filename - the dataset to use;  
	file must be in ARFF format
  	file must be put in folder "data\\Weka_input"
	
	-c int - class index, "first" and "last" are accepted as 	well; "last" is used as default 
 	
	-R int[] - attributes' indexes to ignore during 	classification,  enclosed by double quotes;
 	By default, attributes at indexes 1, 2, 3, 12 are removed 	already
 	
	-W classifier - class' name and options, enclosed by 	double quotes; the classifier to cross-validate
 
Example command-line:
 *  <pre>
 *  	java -jar WekaClassifier.jar -t data.csv -c last -R "13, 14, 15, 16, 17, 18" -W "weka.classifiers.trees.J48 -C 0.25"
 *  </pre>

* Note:
- Classifier's name must be in full form as in example.
- The arff file always contains 7 different labels due to preprocessing algorithm, which is from index 13-> 19. To avoid using unrelated labels, always remove them in command-line, as shown in example.

3. Evaluator.jar
This class is responsible for evaluating Weka classifiers

Command-line parameters:
	-i filename - input CSV data file; 		
	the file must be put in folder "data\\raw_data"

-w filename - input CSV classifier file;
the file must be put in folder "data\\classifiers"

-o filename - output CSV file;
the file will be put in folder "data\\output"

-c int - cutoff increment value, "stdev" is accepted as well; by default, "stdev" will be used 

-d int - number of day for calculating RET
by default, day = 1 will be used 

Example command-line:
* <pre>
* 	java -jar Evaluator.jar -i buyStrategy.csv -o report.csv -w classifiers.csv -c stdev  -d 6
* </pre>

--------------------------------------------------------------
IV. How to use
The program allows users to choose between 3 options:
- If users just want to calculate the labels for classification, they can use Preprocessor.jar to preprocess the raw data.

- If users want to run classification on 1 preprocessed data file, they can use WekaClassifier.jar

- Else if they want to compare a large number of classifiers, Evaluator.jar is a better option than run WekaClassifier.jar multiple times.

* Note:
- Please follow the guide and format of file and command-line to use the programs
